# MARMIT

MARMIT (Multilayer rAdiative tRansfer Model of soIl reflecTance) is a radiative transfer model that predicts the spectral reflectance of a bare soil from 400 nm to 2500 nm with a 1 nm step (solar domain) as a function of its surface water content [1]. MARMIT-2 [2] is the improved version of MARMIT. The 'marmit' and 'marmit2' directories contain some of the databases used for the validation of MARMIT and MARMIT-2, as well as Python codes to simulate the reflectance spectra of wet soils with each version of the model.

![](MARMIT2.png)

## Contacts

Alice Dupiau (dupiau@ipgp.fr), Stéphane Jacquemoud (jacquemoud@ipgp.fr) and Xavier Briottet (xavier.briottet@onera.fr).

## References

[1] Bablet A., Vu P.V.H., Jacquemoud S., Viallefont-Robinet F., Fabre S., Briottet X., Sadeghi M., Whiting M.L., Baret F. and Tian J. (2018), MARMIT: a multilayer radiative transfer model of soil reflectance to estimate surface soil moisture content in the solar domain (400–2500 nm), Remote Sensing of Environment, 217:1-17. https://doi.org/10.1016/j.rse.2018.07.031. \
[2] Dupiau A., Jacquemoud S., Briottet X., Fabre S., Viallefont-Robinet F., Philpot W., Di Biagio C., Hébert H. and Formenti P. (2022), MARMIT-2: an improved version of the MARMIT model to predict soil reflectance as a function of surface water content in the solar domain, Remote Sensing of Environment, 272:112951. https://doi.org/10.1016/j.rse.2022.112951. 