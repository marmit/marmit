"""Simulation of spectral reflectance of wet soils in the solar domain using MARMIT-2

January 26th, 2022
-------------------------------------------------------------------------------------
for any question or request, please contact:

Alice Dupiau
dupiau@ipgp.fr

Stéphane Jacquemoud
jacquemoud@ipgp.fr

Xavier Briottet
xavier.briottet@onera.fr
-------------------------------------------------------------
Run the code 'MARMIT-2_simulation_example.py' for an example of 
wet soil reflectance simulation with MARMIT-2.
-------------------------------------------------------------
"""


import numpy as np
from scipy.special import exp1
from scipy.optimize import minimize


def get_spectrum(n_w, alpha_w, n_i, k_i, Rd, L, eps, d_i, wls):

    """Returns spectra of wet soil given L and eps, parameters of MARMIT model

    INPUTS
    ------
    n_w: array, spectral optical index of water
    alpha_w: array, spectral specific absorption of water in cm-1
    n_i, k_i: floats, real and imaginary parts of refractive index of soil particles
    Rd: array, reflectance of dry soil
    L, eps, and d_i: floats, parameters of MARMIT-2 model
    wls: array, wavelengths in nm

    n, alpha, Rd, and wls should have same length

    OUTPUT
    ------
    Rm: array, reflectance of wet soil
    """
    # imaginary part of refractive index of water
    k_w = alpha_w * wls * 10 ** -7 / (4 * np.pi)
    # complex permittivity of water
    e_w = (n_w + k_w * 1j) ** 2
    # complex permittivity of soil particles
    e_i = (n_i + k_i * 1j) ** 2
    # dielectric average
    e = d_i * e_i + (1 - d_i) * e_w
    # effectice refractive index of the mixture
    n = np.sqrt(e).real
    k = np.sqrt(e).imag
    # effective absorption coefficient
    alpha = 4 * np.pi * k / (wls * 10 ** -7)
    # Fresnel coefficients integrated over the hemisphere
    r12_diffuse = (
        (3 * n ** 2 + 2 * n + 1) / (3 * (n + 1) ** 2)
        - 2 * n ** 3 * (n ** 2 + 2 * n - 1) / ((n ** 2 + 1) ** 2 * (n ** 2 - 1))
        + n ** 2 * (n ** 2 + 1) * np.log(n) / (n ** 2 - 1) ** 2
        - n ** 2 * (n ** 2 - 1) ** 2 * np.log(n * (n + 1) / (n - 1)) / (n ** 2 + 1) ** 3
    )
    t12_diffuse = 1 - r12_diffuse
    r21_diffuse = 1 - (1 - r12_diffuse) / n ** 2
    t21_diffuse = 1 - r21_diffuse
    # transmission of the water layer
    if L > 0:
        Tw_diffuse = (1 - alpha * L) * np.exp(-alpha * L) + (alpha * L) ** 2 * exp1(
            alpha * L
        )
    else:
        Tw_diffuse = np.ones(np.shape(n))
    # reflectance of totally wet soil
    Rw = (
        t12_diffuse
        * t21_diffuse
        * Rd
        * Tw_diffuse ** 2
        / (1 - r21_diffuse * Rd * Tw_diffuse ** 2)
    )
    # mixing the reflectances of wet and dry areas
    Rm = (eps * Rw ** (1 / 2.27) + (1 - eps) * Rd ** (1 / 2.27)) ** 2.27

    return Rm


def sigmoid(x, K, a, psi):

    """ sigmoid of phi """
    y = K / (1 + a * np.exp(-psi * x))

    return y
