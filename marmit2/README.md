# MARMIT-2

MARMIT-2 [1] is an improved version of MARMIT [2] (Multilayer rAdiative tRansfer Model of soIl reflecTance).
This repository contains some of the databases used for the validation of MARMIT-2, as well as Python codes to simulate the reflectance spectra of wet soils. 

## Python codes

The 'MARMIT2_simulation_example' script simulates the reflectance spectra of wet soils. It uses a measured spectrum of a dry soil and computes wet soil spectra for chosen sets of (L, eps, delta). A calibration relation (sigmoid) is associated to each soil sample to relate the soil moisture content (SMC) to the parameters (L, eps). The three sigmoid parameters are provided in the database for each soil sample. The file 'MARMIT2_functions' contains the functions called during the execution of the script.
The following Python packages need to be imported: numpy, math, matplotlib, pandas and scipy. 

## databases

Each of the 8 database directories (Bablet_2016, Dupiau_2020, Humper_2015, Lesaignoux_2008, Liu_2002, Lobell_2002, Marcq_2012 and Philpot_2014) contains:
- a sub-directory 'spectra' whose .csv files contain the reflectance spectra. The first column is the wavelength (in nm) and the second is the value of the reflectance;
- an Excel file that links each .csv reflectance file to a soil sample ID and soil moisture content. It also contains extra information (particle size and/or mineralogical) about the soil samples;
- a .csv file that contains the same information as the second sheet of the Excel file and that is called in the Python code. 

The reflectance of the soil samples of Liu_2002 database were measured in different illumination and measurement configurations. The .csv files in the 'spectra' sub-directory contain the reflectances spectra measured vertically under a 15° illumination. The .csv files in the 'spectra_all_configurations' directory contain the reflectances measured in all other configurations. See the Excel file for more details.

## parameters

This folder contains data needed to run the MARMIT-2 model:
- 'n_segelstein.csv' is the real part of the refractive index of water: the first column is the wavelength (in nm) and the second is the index value measured by [3] interpolated at a 1 nm step;
- 'alpha_buikouwie.csv' is the specific absorption coefficient of water: the first column is the wavelength (in nm) and the second is the coefficient value (in 1/cm). We concatenated three measurements from three articles in the litterature: [4] in the range 400-784 nm, [5] in the range 785-1350 nm and [6] in the range 1351-2500 nm. We then interpolated the values to obtain a 1 nm step.   

All those data can be found at : https://omlc.org/spectra/water/abs/index.html.

## Contacts

Alice Dupiau (dupiau@ipgp.fr), Stéphane Jacquemoud (jacquemoud@ipgp.fr) and Xavier Briottet (xavier.briottet@onera.fr).

## References

[1] Dupiau A., Jacquemoud S., Briottet X., Fabre S., Viallefont-Robinet F., Philpot W., Di Biagio C., Hébert H., Formenti P. (2022), MARMIT-2: an improved version of the MARMIT model to predict soil reflectance as a function of surface water content in the solar domain, Remote Sensing of Environment, 272:112951. https://doi.org/10.1016/j.rse.2022.112951. \
[2] Bablet A., Vu P.V.H., Jacquemoud S., Viallefont-Robinet F., Fabre S., Briottet X., Sadeghi M., Whiting M.L., Baret F. and Tian J. (2018), MARMIT: a multilayer radiative transfer model of soil reflectance to estimate surface soil moisture content in the solar domain (400–2500 nm), Remote Sensing of Environment, 217(1-17). https://doi.org/10.1016/j.rse.2018.07.031. \
[3] Segelstein D.J. (1981), The complex refractive index of water, Master Thesis, University of Missouri-Kansas City.\
[4] Buiteveld H., Hakvoort J.M.H., Donze M. (1994), The optical properties of pure water, in SPIE Proceedings on Ocean Optics XII, edited by J.S. Jaffe, 2258:174-183. https://doi.org/10.1117/12.190060. \
[5] Kou L., Labrie D., Chylek P. (1993), Refractive indices of water and ice in the 0.65-2.5 µm spectral range, Applied Optics, 32:3531-3540. https://doi.org/10.1364/AO.32.003531. \
[6] Wieliczka D.M., Weng S., Querry M.R. (1989), Wedge shaped cell for highly absorbent liquids: infrared optical constants of water, Applied Optics, 28:1714-1719. https://doi.org/10.1364/AO.28.001714.