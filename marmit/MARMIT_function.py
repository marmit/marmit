"""Simulation of spectral reflectance of wet soils in the solar domain

April 26th, 2021
----------------------------------------------------------------------
for any question or request, please contact:

Alice Dupiau
dupiau@ipgp.fr

Stéphane Jacquemoud
jacquemoud@ipgp.fr

Xavier Briottet
xavier.briottet@onera.fr
-------------------------------------------------------------
Run the code 'MARMIT_simulation_example.py' for an example of 
wet soil reflectance simulation with MARMIT.
-------------------------------------------------------------
"""

import numpy as np
from scipy.optimize import minimize

from scipy.special import exp1


def get_spectrum(n, alpha, Rd, L, eps):

    """Returns spectra of wet soil given L and eps, parameters of MARMIT model

    INPUTS
    ------
    n: array, spectral optical index of water
    alpha: array, water absorption spectral coefficient in cm-1
    Rd: array, reflectance of dry soil
    n, alpha and Rd should have same length
    L and eps: floats, parmameters of MARMIT model

    OUTPUT
    ------
    Rm: array, reflectance of wet soil
    """

    r12_diffuse = (
        (3 * n ** 2 + 2 * n + 1) / (3 * (n + 1) ** 2)
        - 2 * n ** 3 * (n ** 2 + 2 * n - 1) / ((n ** 2 + 1) ** 2 * (n ** 2 - 1))
        + n ** 2 * (n ** 2 + 1) * np.log(n) / (n ** 2 - 1) ** 2
        - n ** 2 * (n ** 2 - 1) ** 2 * np.log(n * (n + 1) / (n - 1)) / (n ** 2 + 1) ** 3
    )
    t12_diffuse = 1 - r12_diffuse
    r21_diffuse = 1 - (1 - r12_diffuse) / n ** 2
    t21_diffuse = 1 - r21_diffuse
    if L > 0:
        Tw_diffuse = (1 - alpha * L) * np.exp(-alpha * L) + (alpha * L) ** 2 * exp1(
            alpha * L
        )
    else:
        Tw_diffuse = np.ones(np.shape(n))
    # MARMIT with diffuse light in water layer
    Rw = (
        t12_diffuse
        * t21_diffuse
        * Rd
        * Tw_diffuse ** 2
        / (1 - r21_diffuse * Rd * Tw_diffuse ** 2)
    )
    Rm = eps * Rw + (1 - eps) * Rd

    return Rm


def sigmoid(phi, K, a, psi):

    SMC = K / (1 + a * np.exp(-psi * phi))

    return SMC
