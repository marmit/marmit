"""Simulation of spectral reflectance of wet soils in the solar domain

April 26th, 2021
----------------------------------------------------------------------
for any question or request, please contact:

Alice Dupiau
dupiau@ipgp.fr

Stéphane Jacquemoud
jacquemoud@ipgp.fr

Xavier Briottet
xavier.briottet@onera.fr
------------------------------------------------------------------------
This script generates a plot of spectra simulated with the MARMIT model.
Select soil sample (database and ID).
Then run the code and the plot will appear.
------------------------------------------------------------------------
"""

import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd

print("Simulation in progress, please wait.")
import MARMIT_function as func

## Select database and load data
database = "Bablet_2016"
path = "marmit/databases/" + database + "/" + database + ".csv"
df = pd.read_csv(path)
# read first spectrum from database to determine wavelength range
filename = "marmit/databases/" + database + "/spectra/" + df.Refl_file.iloc[0]
R = pd.read_csv(filename, sep="\t")
# lower wavelength bound is set to 400 nm because the optical
# index of water is measured only starting from 400 nm
wlmin = max(R.Wvl.min(), 400)
wlmax = R.Wvl.max()
wls = np.arange(wlmin, wlmax + 1, 1)

## Constant parameters of MARMIT model
# optical index of water (Segelstein, 1981)
n = pd.read_csv("marmit/parameters/n_segelstein.csv", sep="\t", index_col="Wvl")
n = np.array(n.loc[wlmin:wlmax].iloc[:, 0])
# absorption coefficient of water, cm-1 (Buiteveld/Kou/Wieliczka)
alpha = pd.read_csv("marmit/parameters/alpha_buikouwie.csv", sep="\t", index_col="Wvl")
alpha = np.array(alpha.loc[wlmin:wlmax].iloc[:, 0])

"""Select soil ID

Bablet_2016: ID between 1 and 17
Humper_2015: ID between 1 and 57
Lesaignoux_2008:ID between 1 and 32
Liu_2002: ID between 1 and 92
Lobell_2002: ID between 1 and 4
Marcq_2012: ID between 1 and 9
Philpot_2014: ID between 1 and 3
"""

ID = 1
df1 = df[df.ID == ID]

## Read and plot dry soil reflectance
Rd_filename = (
    "marmit/databases/"
    + database
    + "/spectra/"
    + df1.Refl_file[df1.SMCg == df1.SMCg.min()].iloc[0]
)
Rd = pd.read_csv(Rd_filename, sep="\t", index_col="Wvl")
Rd = np.array(Rd.loc[wlmin:wlmax].iloc[:, 0])
plt.figure()
plt.plot(wls, Rd, color="black", label="dry soil (measure)")

## Fetch sigmoid parameters
K = float(df1.K.unique())
a = float(df1.a.unique())
psi = float(df1.psi.unique())

## Plot simulated spectra for varying L and eps
# colors for plots
nb_colors = 6
colors = cm.jet(np.linspace(0, 1, nb_colors))
i = 0
# varying eps and constant L
L = 0.001  # L: thickness of water layer in cm
for eps in [0.3, 0.5, 0.7]:
    Rw = func.get_spectrum(n, alpha, Rd, L, eps)
    phi = L * eps
    SMC = func.sigmoid(phi, K, a, psi)  # Soil Moisture Content
    plt.plot(
        wls,
        Rw,
        label="L=%.3f cm; $\epsilon$=%.2f ; SMCg=%.2f" % (L, eps, SMC),
        color=colors[i],
    )
    i += 1
# varying L and constant eps
eps = 1  # eps: wet soil surface ratio, values between 0 and 1
for L in [0.001, 0.01, 0.05]:
    Rw = func.get_spectrum(n, alpha, Rd, L, eps)
    phi = L * eps
    SMC = func.sigmoid(phi, K, a, psi)  # Soil Moisture Content
    plt.plot(
        wls,
        Rw,
        label="L=%.3f cm; $\epsilon$=%.2f ; SMCg=%.2f" % (L, eps, SMC),
        color=colors[i],
    )
    i += 1
plt.xlabel("Wavelength (nm)")
plt.ylabel("Reflectance")
plt.legend()
plt.title("Database: " + database + "\nSoil: " + str(df1.Name.iloc[0]))

print("End of simulation, you can close the figure.")

plt.show()

